;; stumpwm configuration file begins

(in-package :stumpwm)
;;(run-shell-command "xsetroot -solid darkgray")

(defun show-battery-charge ()
  (let ((raw-battery (run-shell-command "acpi | cut -d, -f2 | sed s/[[:space:]]//" t)))
    (substitute #\Space #\Newline raw-battery)))

(defun show-battery-state ()
  (let ((raw-battery (run-shell-command "acpi | cut -d: -f2 | cut -d, -f1 | sed 's/[ \t]*//;s/[ \t]*$//'" t)))
    (substitute #\Space #\Newline raw-battery)))

;; al/... functions taken from https://github.com/alezost/stumpwm-config/blob/master/visual.lisp

(defun al/ml-string (str &key (fg "7") bg (bright nil bright-set))
  "Make STR a mode-line string with FG and BG colors.
FG and BG can be nil or a string containing either a single digit (a
number from `*colors*' list) or #XXXXXX value.
If BRIGHT is set and is non-nil, use bright color."
  ;; See (info "(stumpwm) Colors") for details on the color machinery.
  (let ((fc (and (stringp fg)
                 (concat "^(:fg "
                         (if (= 1 (length fg))
                             fg
                             (concat "\"" fg "\""))
                         ")")))
        (bc (and (stringp bg)
                 (concat "^(:bg "
                         (if (= 1 (length bg))
                             bg
                             (concat "\"" bg "\""))
                         ")"))))
    (concat "^[" (and bright-set (if bright "^B" "^b"))
            fc bc str "^]")))

(defun al/ml-window-class (&optional (str " %c "))
  "Window class color construct for mode-line and window list."
  (al/ml-string str :fg "#4c83ff" :bg "#333333"))

(defun al/time (&optional (str "%k:%M"))
  "Window class color construct for mode-line and window list."
  (al/ml-string str :fg "#4c83ff" :bg "#000000"))


(defcommand emacs () ()
  "Start emacs unless it is already running, in which case focus it."
  (run-or-raise "emacsclient -c -a 'emacs'" '(:class "Emacs")))

(defcommand dvorakbr () ()
	    "switch to dvorak br layout whenever necessary."
	    (run-shell-command "setxkbmap br dvorak && xmodmap ~/.Xmodmap"))
(defcommand dvorakus () ()
	    "switch back to dvorak us."
	    (run-shell-command "setxkbmap us dvorak && xmodmap ~/.Xmodmap"))

(defvar al/keyboard-layout (make-sparse-keymap))
(define-key *root-map* (kbd "f") 'al/keyboard-layout)
(define-key al/keyboard-layout (kbd "b") "dvorakbr")
(define-key al/keyboard-layout (kbd "u") "dvorakus")

;; root-map
(undefine-key *root-map* (kbd "c"))
(define-key *root-map* (kbd "c") "exec urxvt")
(define-key *root-map* (kbd "slash") "remove-split")
(define-key *root-map* (kbd "s-F11") "frame-windowlist")
;; top-map
(define-key *top-map* (kbd "s-F9") "exec mpc toggle")
(define-key *top-map* (kbd "s-F7") "exec volume_dec")
(define-key *top-map* (kbd "s-F8") "exec volume_inc")
(define-key *top-map* (kbd "s-F5") "exec light -U 10")
(define-key *top-map* (kbd "s-F6") "exec light -A 10")
(define-key *top-map* (kbd "s-F10") "mode-line")
(define-key *top-map* (kbd "s-n") "gnext")
(define-key *top-map* (kbd "s-space") "gprev")
(define-key *top-map* (kbd "s-s") "exec xdotool type $(grep -v '^#' ~/.local/share/snippets | dmenu -nb black -nf pink -sb pink -sf black -i -l 50 | cut -d' ' -f1)")
(define-key *top-map* (kbd "s-M-w") "pull-from-windowlist")
(define-key *top-map* (kbd "s-f") "ratrelwarp 10 0")
(define-key *top-map* (kbd "s-b") "ratrelwarp -10 0")
(define-key *top-map* (kbd "s-p") "ratrelwarp 0 -10")
(define-key *top-map* (kbd "s-d") "ratrelwarp 0 10")
(define-key *top-map* (kbd "s-c") "ratclick")


;; variable assignment
;; (set-font "-schumacher-clean-medium-r-normal--16-160-75-75-c-80-iso646.1991-irv")
;;(set-font "-misc-*-medium-r-*-*-18-*-*-*-*-*-iso8859-1")
;;(bar (show-battery-charge) 10 #\= #\Space)
(set-module-dir "/home/chisa/.stumpwm.d/modules/")
;;(setf *hidden-window-color* "^B^70")
(setf *shell-program* (getenv "SHELL"))
(setf *mode-line-timeout* 1)
(setf *mode-line-background-color* "#000000")
(setf *mode-line-foreground-color* "#FFFFFF")
(setf *window-format* (al/ml-window-class))
;; There's a function to customize the time display color.
;; What follows was done only for learning purposes.
(setf *time-modeline-string* (concat "%a %b %e" "^[^(:fg \"#4c83ff\")" "^(:bg \"#000000\") %k:%M^]"))
(setf *screen-mode-line-format* (list "[%n] " "%v" "^>[ %d " '(:eval (show-battery-charge)) '(:eval (show-battery-state))
				      "]" "^(:fg \"#4c83ff\") ^(:bg \"#000000\")" '(:eval (run-shell-command "mpc current" t))))

(setf *window-border-style* :thin)
(toggle-mode-line (current-screen) (current-head))
;; groups
(grename "Alpha")
(gnewbg "Beta")
(gnewbg "Tau")
(gnewbg "Pi")
(gnewbg "Zeta")

(load-module "screenshot")
(load-module "stump-lock")

;; stump-lock
(setf stump-lock:*password* "123456")
;;(define-key *top-map* (kbd "s-l") "lock-screen")
(load "~/quicklisp/setup.lisp")
(load-module "ttf-fonts")
(ql:quickload :clx-truetype)
(clx-truetype:cache-fonts)

(set-font (make-instance 'xft:font
			 :family "Sazanami Gothic"
                         :subfamily "Gothic-Regular"
                         :size 14
                         :antialias t))

;; EOF
